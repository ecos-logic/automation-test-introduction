import com.codeborne.selenide.SelenideElement;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;


@Slf4j
class SelenideTest {
    @Test
    void searchBaeldung() {
        open("https://duckduckgo.com/");

        SelenideElement searchBox = $(By.id("searchbox_input"));
        searchBox.click();
        searchBox.sendKeys("Baeldung");
        searchBox.pressEnter();

        SelenideElement firstResult = $(By.id("r1-0"));
        firstResult.shouldHave(text("Baeldung"));
    }
}
